#!/usr/bin/env python 
# -*- coding:utf-8 -*-
# Author : Zhenglong Sun
# Data : 2022-1-7 16:12
import pandas as pd
import numpy as np
import torch
import torch.nn as nn
from torch.utils.data import DataLoader, random_split
from torchvision import transforms
import pfdataset as pfd
import matplotlib.pyplot as plt
import torchvision.transforms as transforms
from sklearn.metrics import classification_report, accuracy_score
from torch.optim import lr_scheduler

# Device configuration
device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')

# Hyper-parameters
input_size = 60 * 5  # samples*pmu signals 15*19 for 118 bus system.  15*5 for 39
hidden_size = 256
num_classes = 6
num_epochs =100
batch_size = 100
learning_rate = 0.0001
classes = ('generator trip', 'load disconnection', 'fault', 'line trip',
           'frequency rampup', 'frequency rampdown')
# parameters for dataset processing
df_path = r'C:/Users/Warrior/Desktop/python 3.4 projects/scripts/frequency total_dataset with 6 events for 39 bus ' \
          r'system.pkl'

end_time_index = 85
initial_time_index = 25
composed = transforms.Compose([pfd.ToTensor(), pfd.LpNormalize(dim=0), pfd.StdNormalize(dim=0)])
#composed = transforms.Compose([pfd.ToTensor(),  pfd.LpNormalize(dim=0)])
# create dataset
dataset = pfd.FEDataset(df_path, initial_time_index, end_time_index, transform=composed, noise=False, snr_min=20,
                        snr_max=50)

# split the train and test dataset
train_dataset, test_dataset = random_split(dataset, [round(0.8 * dataset.__len__()), round(0.2 * dataset.__len__())],
                                           generator=torch.Generator().manual_seed(7))

# get first sample and unpack
# first_data = dataset[0]
# features, labels = first_data
# print(features, labels)

# Data loader
# has shuffled in df
train_loader = torch.utils.data.DataLoader(dataset=train_dataset,
                                           batch_size=batch_size,
                                           shuffle=True)

test_loader = torch.utils.data.DataLoader(dataset=test_dataset,
                                          batch_size=batch_size,
                                          shuffle=False)


# examples = iter(test_loader)
# example_data, example_targets = examples.next()

# for i in range(6):
#     print(example_targets[i])
#     plt.subplot(2,3,i+1)
#     plt.plot(example_data[i][0:100], 'ro')
# plt.show()

# Fully connected neural network with one hidden layer
class NeuralNet(nn.Module):
    def __init__(self, input_size, hidden_size, num_classes):
        super(NeuralNet, self).__init__()
        self.layers = nn.Sequential(
            nn.Linear(input_size, hidden_size),
            nn.Dropout(p=0.5),
            nn.ReLU(),
            nn.Linear(hidden_size, 256),
            nn.Dropout(p=0.5),
            nn.ReLU(),
            nn.Linear(256, 64),
            nn.Dropout(p=0.8),
            nn.ReLU(),
            nn.Linear(64, num_classes)
        )

    def forward(self, x):
        out = self.layers(x)
        # no activation and no softmax at the end
        return out


model = NeuralNet(input_size, hidden_size, num_classes).to(device)


def weights_init(m):
    if isinstance(m, torch.nn.Conv2d) or isinstance(m, torch.nn.Linear):
        torch.nn.init.xavier_uniform_(m.weight)
        if m.bias is not None:
            torch.nn.init.zeros_(m.bias)


model.apply(weights_init)
# Loss and optimizer
criterion = nn.CrossEntropyLoss()
optimizer = torch.optim.Adam(model.parameters(), lr=learning_rate)
# optimizer = torch.optim.SGD(model.parameters(), lr=learning_rate)
# StepLR Decays the learning rate of each parameter group by gamma every step_size epochs
scheduler = lr_scheduler.StepLR(optimizer, step_size=10, gamma=1)  # 相当于不衰减

# for drawing
train_loss_all = []
train_acc_all = []
test_loss_all = []
test_acc_all = []

# Train the model
n_total_steps = len(train_loader)
for epoch in range(num_epochs):
    print("Epoch {}/{}".format(epoch, num_epochs - 1))
    model.train()  # 模式设为训练模式
    train_loss = 0
    corrects = 0
    train_num = 0
    for i, (images, labels) in enumerate(train_loader):
        # origin shape: [100, 1, 28, 28]
        # resized: [100, 784]
        images = images.reshape(-1, input_size).to(device)
        labels = labels.type(torch.LongTensor)  # 这个很重要，必须有，否则出错
        labels = labels.to(device)

        # Forward pass
        outputs = model.forward(images.float())  # 也可以model(images)
        pre_lab = torch.argmax(outputs, 1)
        loss = criterion(outputs, labels)

        # Backward and optimize
        optimizer.zero_grad()
        loss.backward()
        optimizer.step()

        train_loss += loss.item() * images.size(0)
        corrects += torch.sum(pre_lab == labels.data)
        train_num += images.size(0)
        # LR Decays
        scheduler.step()

    train_loss_all.append(train_loss / train_num)
    train_acc_all.append(corrects.double().item() / train_num)
    print("Epoch{}, Train Loss: {:.4f} Train Acc: {:.4f}".format(epoch, train_loss_all[-1], train_acc_all[-1]))
    #### 验证集测试开始 ####
    # 设置模式为验证模式
    model.eval()
    corrects, test_num, test_loss = 0, 0, 0

    for i, (images, labels) in enumerate(test_loader):
        images = images.reshape(-1, input_size).to(device)
        labels = labels.type(torch.LongTensor)  # 这个很重要，必须有，否则出错
        labels = labels.to(device)
        # Forward pass
        outputs = model.forward(images.float())  # 也可以model(images)
        pre_lab = torch.argmax(outputs, 1)
        loss = criterion(outputs, labels)
        test_loss += loss.item() * images.size(0)
        corrects += torch.sum(pre_lab == labels)
        test_num += images.size(0)

    # 计算经过一个epoch的训练后再测试集上的损失和精度
    test_loss_all.append(test_loss / test_num)
    test_acc_all.append(corrects.double().item() / test_num)

    print("Epoch{} Test Loss: {:.4f} Test Acc: {:.4f}".format(epoch, test_loss_all[-1], test_acc_all[-1]))

plt.figure(figsize=[14, 5])
plt.subplot(1, 2, 1)
plt.plot(train_loss_all, "ro-", label="Train Loss")
plt.plot(test_loss_all, "bs-", label="Val Loss")
plt.legend()
plt.xlabel("epoch")
plt.ylabel("Loss")

plt.subplot(1, 2, 2)
plt.plot(train_acc_all, "ro-", label="Train Acc")
plt.plot(test_acc_all, "bs-", label="Test Acc")
plt.xlabel("epoch")
plt.ylabel("Acc")
plt.legend()

plt.show()

# 最后输出模型的精度

predict_labels = []
true_labels = []

for step, (images, labels) in enumerate(test_loader):
    images = images.reshape(-1, input_size).to(device)
    labels = labels.type(torch.LongTensor)  # 这个很重要，必须有，否则出错
    labels = labels.to(device)
    # Forward pass
    outputs = model.forward(images.float())  # 也可以model(images)
    pre_lab = torch.argmax(outputs, 1)
    predict_labels += pre_lab.flatten().tolist()
    true_labels += labels.flatten().tolist()

print(classification_report(predict_labels, true_labels, digits=4))
print("Accuracy of the network：", accuracy_score(predict_labels, true_labels))

figure_data = pd.concat(
    [pd.DataFrame({'train_loss_all': train_loss_all}), pd.DataFrame({'test_loss_all': test_loss_all}),
     pd.DataFrame({'train_acc_all': train_acc_all}), pd.DataFrame({'test_acc_all': test_acc_all}),
     pd.DataFrame({'predict_labels': predict_labels}), pd.DataFrame({'true_labels': true_labels})]
    , axis=1)
figure_data.to_csv(
    r'C:\Users\Warrior\Desktop\pedg paper\images\FNN figure data of voltage classification for 39 bus system with '
    r' no noise '
    r'25-45.csv')
