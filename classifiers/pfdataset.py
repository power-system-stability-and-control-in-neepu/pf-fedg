#!/usr/bin/env python 
# -*- coding:utf-8 -*-
# Author : Zhenglong Sun
# Data : 2022-3-15 22:43
import torch
from torch.utils.data import Dataset
import numpy as np
import pandas as pd
import torch.nn.functional as F

def wgn(x, snr_min,snr_max):
    snr=np.random.randint(snr_min,snr_max)
    snr = 10**(snr/10.0)
    xpower = np.sum(x**2)/len(x)
    npower = xpower / snr
    return np.random.randn(len(x)) * np.sqrt(npower)

class FEDataset(Dataset):

    def __init__(self,df_path, initial_time_index, end_time_index, transform=None,noise=False, snr_min=30,snr_max=50):
        # Initialize data, download, etc.
        # read with pandas

        df = pd.read_pickle(df_path)
        result_series = df['Result Data Frame']
        all_exa = np.zeros(shape=(result_series.shape[0], end_time_index-initial_time_index, result_series[0].values.shape[1]-1))
        for i in range(result_series.shape[0]):
            one_ex = result_series[i].values[initial_time_index:end_time_index, 1:] #去除时间列
            if noise:
                for col in range(one_ex.shape[1]):
                    one_ex[:, col] = one_ex[:, col]+ wgn(one_ex[:, col], snr_min, snr_max)
                else:
                    pass
            all_exa[i, :, :] = one_ex
        #all_exa.astype(np.float64)
        labels_series = df['event label']
        all_lables = np.array([labels_series], dtype=np.float32).T
        self.n_samples = all_exa.shape[0]  #这个必须有，结构继承要求的
        # all_exa, all_labels is np array
        self.x_data = all_exa  # size [n_samples, n_features]
        self.y_data = all_lables  # size [n_samples, 1]

        self.transform = transform
    # support indexing such that dataset[i] can be used to get i-th sample
    def __getitem__(self, index):

        sample = self.x_data[index], self.y_data[index].squeeze()
       # 添加这一行，用于降维度（从 torch.Size([2, 1]) 降成torch.Size([2]) ） ，即 target 从[ [4], [4]] 降成 [ 4, 4 ]

        if self.transform:
            sample = self.transform(sample)

        return sample

    def __len__(self):
        return self.n_samples

# we can also use the transforms in torch vision
class ToTensor:
    # Convert ndarrays to Tensors
    def __call__(self, sample):
        inputs, targets = sample
        return torch.from_numpy(inputs).float(), torch.from_numpy(targets)


class ToImage:
    # unsqueeze the dim of inputs to 3
    def __init__(self, size):
        self.size = size
    def __call__(self, sample):
        inputs, targets = sample
        if self.size == 0:
            pass
        else:
            inputs = inputs.view(self.size, -1)
        inputs = torch.unsqueeze(inputs, dim=0)
        return inputs, targets


class MulTransform:
    # multiply inputs with a given factor
    def __init__(self, factor):
        self.factor = factor

    def __call__(self, sample):
        inputs, targets = sample
        inputs *= self.factor
        return inputs, targets


class LpNormalize:
    # Performs Lp normalization of inputs over specified dimension,默认按行。
    def __init__(self, p=2, dim =1):
        self.p = p
        self.dim = dim

    def __call__(self, sample):
        inputs0, targets = sample
        inputs = F.normalize(inputs0, self.p, self.dim )
        return inputs, targets

class StdNormalize:
    # Performs Lp normalization of inputs over specified dimension,默认按行。
    def __init__(self, dim):
        self.dim = dim

    def __call__(self, sample):
        inputs0, targets = sample
        mean=torch.mean(inputs0,dim=self.dim)#[bsize]
        std=torch.std(inputs0,dim=self.dim)#[bsize]
        return (inputs0-mean.unsqueeze(self.dim))/std.unsqueeze(self.dim), targets




