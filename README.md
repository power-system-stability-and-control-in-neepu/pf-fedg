# PF-FEDG

an open-source frequency event data generator based on PowerFactory (PF-FEDG)

The code in the paper "PF-FEDG: An open-source data generator for frequency disturbance event detection with deep-learning reference classifiers".<br>The papaer link is: https://www.sciencedirect.com/science/article/pii/S2352484722025707<br>
Sun Z.,Machlev R.,Jiang C.,Perl M.,Belikov J.,Levron Y. (2023). PF-FEDG: An open-source data generator for frequency disturbance event detection with deep-learning reference classifiers. Energy Reports, 9, 397-413.

PF-FEDG could produce various labeled FDEs covering the literature  
Three reference FDEs classifiers based on deep learning techniques are also proposed in PF-FEDG  
Performance of the models are tested on IEEE 39-bus system and IEEE 118-bus system(tesy system models are also provided)


# Python Environment 

Power Factory 15.2 + Python 3.4 is used for generate the original pkl file

Python 3.8 is used to test the datasets and detect frequency disturbance events

# Overall framework of PF-FEDG package

![Overall framework of PF-FEDG package](/images/structures.png)

# Frequency disturbance events supported in PF-FEDG

- generator trip (GT)
- load disconnection (LD)
- line outage (LO)
- frequency ramping up (FRU)
- frequency ramping down (FRD)
- frequency oscillation (FO) caused by fault

# Features for frequency disturbance events detection

- bus frequency
- bus voltage amplitude
- ROCOF
- bus relative angle shift

# Deep-learning reference classifiers


- FNN based classifiers
- CNN based classifiers
- BiLSTM based classifiers

# Performance of classifiers

<!-- ![](https://gitlab.com/power-system-stability-and-control-in-neepu/pf-fedg/-/raw/main/images/ieee%2039%2020.png) -->
<img src="/images/ieee%2039%2020.png"  width="850">

<!-- ![](https://gitlab.com/power-system-stability-and-control-in-neepu/pf-fedg/-/raw/main/images/ieee%2039%20150.png)

![](https://gitlab.com/power-system-stability-and-control-in-neepu/pf-fedg/-/raw/main/images/loss%20and%20acc%20FNN%20figure%20data%20of%20rocof%20classification%20for%2039%20bus%20system%20with%20no%20noise%2025-175.png)

![](https://gitlab.com/power-system-stability-and-control-in-neepu/pf-fedg/-/raw/main/images/Confusion%20FNN%20figure%20data%20of%20rocof%20classification%20for%2039%20bus%20system%20with%20no%20noise%2025-175.png) -->

<img src="/images/ieee%2039%20150.png"  width="850">

<img src="/images/loss%20and%20acc%20FNN%20figure%20data%20of%20rocof%20classification%20for%2039%20bus%20system%20with%20no%20noise%2025-175.png"  width="600">

<img src="/images/Confusion%20FNN%20figure%20data%20of%20rocof%20classification%20for%2039%20bus%20system%20with%20no%20noise%2025-175.png"  width="600">
