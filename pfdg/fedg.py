#!/usr/bin/env python 
# -*- coding:utf-8 -*-
# Author : Zhenglong Sun
# Data : 2021-12-8 11:45
import numpy as np
import pandas as pd
import sys
import os


class frequency_event_dg:

    def __init__(self, pf_install_path, project_name, monitored_buses, monitored_variables):
        # monitored_variables is a list，which could include'frequency'、'voltage angle'、'rocof' or 'voltage'
        sys.path.append(pf_install_path + r'\Python\3.4')
        import powerfactory
        self.app = powerfactory.GetApplication()
        self.app.ActivateProject(project_name)
        self.dic_mv = {}
        if 'all buses' in monitored_buses:
            self.dic_mv['*.ElmTerm'] = []
        else:
            for ele in monitored_buses:
                self.dic_mv[ele+'.ElmTerm'] = []
        for i in range(len(monitored_variables)):
            if monitored_variables[i] not in ['frequency','voltage angle','rocof','voltage']:
                print('WARNING:Please check the input of monitored_variables!')
                sys.exit()
        if 'frequency' in monitored_variables:
            for ele in self.dic_mv.keys():
                self.dic_mv[ele].append('m:fe')
        if 'voltage' in monitored_variables:
            for ele in self.dic_mv.keys():
                self.dic_mv[ele].append('m:u')
        if 'voltage angle' in monitored_variables:
            for ele in self.dic_mv.keys():
                self.dic_mv[ele].append('m:phiu')
        if 'rocof' in monitored_variables:
            for ele in self.dic_mv.keys():
                self.dic_mv[ele].append('m:dfedt')
        res = self.app.GetFromStudyCase('All calculations.ElmRes')
        monitor_folder = res.GetContents()
        for intmon in monitor_folder:
            intmon.Delete()
        for ele in self.dic_mv.keys():
            elements = self.app.GetCalcRelevantObjects(ele)
            for element in elements:
                res.AddVars(element, *self.dic_mv[ele])

    def get_ini_load(self):
        iniloadP = []
        iniloadQ = []
        load_name = []
        loads = self.app.GetCalcRelevantObjects('*.ElmLod')
        for load in loads:
            load_name.append(load.loc_name)
            iniloadP.append(load.plini)
            iniloadQ.append(load.qlini)
        return [load_name, iniloadP, iniloadQ]

    def generate_loads(self,load_cases_for_each_level, load_level_list, load_variance, load_name, iniloadP, iniloadQ):
        """
    This function is used to generate various P and Q for all the loads, which behave a standard Gaussian Distribution. It will return a load Dataframe with rows corresponses with the generating_cases and columns in the array corresponse with the loads in GetCalcRelevantObjects('*.ElmLod').The original load conditions is included in the returned.
        :param load_cases:The number of created cases with various loads
                """
        loads = self.app.GetCalcRelevantObjects('*.ElmLod')
        load_name_t = []
        for load in loads:
            load_name_t.append(load.loc_name)
        if load_name_t != load_name:
            print('Something wrong with the load scenarios!')
            sys.exit(0)
        Load_temp = np.empty([1,len(loads)])
        tempP = iniloadP
        tempQ = iniloadQ
        for load_level in load_level_list:
            P_ave = load_level * np.array(tempP)
            Q_ave = load_level * np.array(tempQ)
            P_cov = load_variance * np.identity(len(tempP))
            Q_cov = load_variance * np.identity(len(tempQ))
            loadsP0 = np.random.multivariate_normal(list(P_ave), P_cov, load_cases_for_each_level - 1)
            loadsQ0 = np.random.multivariate_normal(list(Q_ave), Q_cov, load_cases_for_each_level - 1)
            loadsP = np.append(loadsP0, [P_ave], axis=0)
            loadsQ = np.append(loadsQ0, [Q_ave], axis=0)
            Load = loadsP + loadsQ * 1j
            Load_array = np.append(Load_temp, Load, axis=0)
            Load_temp = Load_array
        loaddf = pd.DataFrame(Load_array[1:,:], columns=load_name)
        loaddf = loaddf.reset_index(drop=True)
        return loaddf

    def set_loads(self, P, Q,load_name):
        """
    This function will change the P,Q of loads in the active tests system.
        :param P: active power,real elements in load df corresponse with the loads in GetCalcRelevantObjects('*.ElmLod')
        :param Q: reactive power,imaginary elements in  load df corresponse with the loads in GetCalcRelevantObjects('*.ElmLod')
    """
        loads = self.app.GetCalcRelevantObjects('*.ElmLod')
        load_name_t = []
        for load in loads:
            load_name_t.append(load.loc_name)
        if load_name_t != load_name:
            print('Something wrong with the load scenarios!')
            sys.exit(0)
        for load in loads:
            load.plini = P[loads.index(load)]
            load.qlini = Q[loads.index(load)]

    def delete_all_events(self):
        """
    This function will delete all the simulation events or all the contents in the Simulation Event folder
        """
        events = self.app.GetFromStudyCase('Simulation Events/Fault.IntEvt')
        event_folder = events.GetContents()
        for event in event_folder:
            event.Delete()

    def creat_events(self, evt_type, name):
        """
    This function will creat simulation events in digsilent. It returns diffenent eventset object.
        :param evt_type: event type in digsilent,such as 'EvtShc','EvtGen','EvtLod'......
        :param name: string, name of the event
        """
        Sim_event_folder = self.app.GetFromStudyCase('IntEvt')
        Sim_event_folder.CreateObject(evt_type, name)
        event = Sim_event_folder.GetContents(name + '.' + evt_type)[0][0]
        return event

    def set_clear_fault(self, event, element, clear_time):
        """
        This function will clear the fault of the grid element
        :param eventset: event object of creat_events(evt_type,name):
        :param element:  clear the fault where it occurs
        :param clear_time: fault clear time
        """
        event.i_shc = 4
        event.time = clear_time
        event.p_target = element

    def ini_and_sim(self, sampling_step, sim_time, sim_method, net_rep, record_start_time):
        """
    This function will complete the initialization and start the simulation in Digsilent.The event object IntEvt, which contains different envents created func creat_eventsby and the name of IntEvt must be Simulation Events/Fault.Results objective, which contains the variables list and simulation results, and the name must be All calculations by default/
        :param sim_method: Simulation method, 'rms' for electromechanical transients
        :param net_rep: network representation for initialization, unbalanced,3 -phase="rst" balanced=“sym”
        :param record_start_time: the starting time for recording in the simulation results file
        :param sampling_step: the sampling time for the simulation results file
        :param sim_time: the time that the simulation stops or lasts
        """
        # simulation initialization
        # it will execute the power flow automatically
        init = self.app.GetFromStudyCase("ComInc")
        init.iopt_sim = sim_method
        init.iopt_net = net_rep
        init.tstart = record_start_time
        init.dtout = sampling_step
        if init.p_resvar.loc_name == 'All calculations' and init.p_event.loc_name == 'Simulation Events/Fault':
            init.Execute()
        else:
            print(
                'The program is exited.\n Please set "All calculations" as the "Elmres" and "Simulation Events/Fault" as the "IntEvt"!')
            sys.exit(0)
        # start simulation
        sim = self.app.GetFromStudyCase("ComSim")
        sim.tstop = sim_time
        sim.Execute()

    def get_results(self):
        """
    This function will get the complex voltages of each bus
        :return: results is a dataframe.
        """
        res = self.app.GetFromStudyCase('All calculations.Elmres')
        export = self.app.GetFromStudyCase('ASCII Result Export.ComRes')
        export.pResult = res
        export.iopt_csel = 0
        export.iopt_exp = 6
        export.iopt_tsel = 0
        temppath = os.getcwd() + r'\temp_results.csv'
        export.f_name = temppath
        export.Execute()
        # read two rows to get the colums_name
        title = pd.read_csv(temppath, header=None, nrows=2)
        names = title.T[0] + '///' + title.T[1]
        results = pd.read_csv(temppath, header=None, skiprows=2)
        results.columns = names
        results = results.sort_index(axis=1)
        os.remove(temppath)
        return results

    def set_event(self, event, element, occur_time):
        """
        set three phase short circuit fault function.
        :param event: the created event by func creat_events.
        :param element: the element where the fault is applied
        :param occur_time: the occurrence time of fault
        :param fault_type: the fault type, 0 for 3-Phase Short-Circuit

        """

        event.i_shc = 0
        event.time = occur_time
        event.p_target = element
        event.R_f = 0

    def generate_generator_trip_event(self, ini_load, load_cases_for_each_level, load_level_list, load_variance, sampling_step, sim_time, generator_trip_time, event_label):
        loaddf = self.generate_loads(load_cases_for_each_level, load_level_list, load_variance, load_name=ini_load[0], iniloadP=ini_load[1], iniloadQ=ini_load[2])
        data_list = []
        print('Starts generating generator trip event dataset:')
        for i in range(loaddf.shape[0]):
            load_case_index = i + 1
            print('Calculating generator trip event in {} load scenario.'.format(load_case_index))
            P = loaddf.iloc[i,].real
            Q = loaddf.iloc[i,].imag
            self.set_loads(P, Q, ini_load[0])
            generators = self.app.GetCalcRelevantObjects('*.ElmSym')
            for generator in generators:
                self.delete_all_events()
                gt_event = self.creat_events('EvtOutage','Generator_Trip')
                gt_event.time = generator_trip_time
                gt_event.p_target = generator
                gt_event.i_what = 0
                self.ini_and_sim(sampling_step, sim_time, sim_method='rms', net_rep='sym', record_start_time=0)
                temp_list = []
                result_df = self.get_results()
                temp_list.append(result_df)
                temp_list.append(event_label)
                temp_list.append('Trip '+generator.loc_name)
                temp_list.append('Trip ' + generator.loc_name+' at'+ str(generator_trip_time)+' s')
                temp_list.append('Generator trip load case  '+str(load_case_index))
                data_list.append(temp_list)
                self.app.ResetCalculation()
        col_name = ['Result Data Frame', 'event label', 'event position', 'event information', 'load scenario index']
        data_df = pd.DataFrame(data_list, columns=col_name, dtype='object')
        return data_df, loaddf

    def generate_load_disconnection_event(self, ini_load, load_cases_for_each_level, load_level_list, load_variance, sampling_step, sim_time, load_disconnection_time, event_label):
        loaddf = self.generate_loads(load_cases_for_each_level, load_level_list, load_variance, load_name=ini_load[0], iniloadP=ini_load[1], iniloadQ=ini_load[2])
        data_list = []
        print('Starts generating load disconnection event dataset:')
        for i in range(loaddf.shape[0]):
            load_case_index = i + 1
            print('Calculating load disconnection event in {} load scenario.'.format(load_case_index))
            P = loaddf.iloc[i,].real
            Q = loaddf.iloc[i,].imag
            self.set_loads(P, Q, ini_load[0])
            loads = self.app.GetCalcRelevantObjects('*.ElmLod')
            for load in loads:
                if load.typ_id:
                    pass
                else:
                    print('There is no load type in {}！'.format(load.loc_name))
                    sys.exit()
                self.delete_all_events()
                ld_event = self.creat_events('EvtOutage','Load_disconnection')
                ld_event.time = load_disconnection_time
                ld_event.p_target = load
                ld_event.i_what = 0
                self.ini_and_sim(sampling_step, sim_time, sim_method='rms', net_rep='sym', record_start_time=0)
                temp_list = []
                result_df = self.get_results()
                temp_list.append(result_df)
                temp_list.append(event_label)
                temp_list.append('Disconnect '+load.loc_name)
                temp_list.append('Disconnect ' + load.loc_name+' at'+ str(load_disconnection_time)+' s')
                temp_list.append('Load disconnection load case  '+str(load_case_index))
                data_list.append(temp_list)
                self.app.ResetCalculation()
        col_name = ['Result Data Frame', 'event label', 'event position', 'event information', 'load scenario index']
        data_df = pd.DataFrame(data_list, columns=col_name, dtype='object')
        return data_df, loaddf

    def generate_oscillation_event(self, ini_load, load_cases_for_each_level, load_level_list, load_variance, sampling_step, sim_time, fault_execution_time, fault_clear_time, event_label):
        loaddf = self.generate_loads(load_cases_for_each_level, load_level_list, load_variance, load_name=ini_load[0], iniloadP=ini_load[1], iniloadQ=ini_load[2])
        data_list = []
        print('Starts generating oscillation event dataset:')
        for i in range(loaddf.shape[0]):
            load_case_index = i + 1
            print('Calculating oscillation event in {} load scenario.'.format(load_case_index))
            P = loaddf.iloc[i,].real
            Q = loaddf.iloc[i,].imag
            self.set_loads(P, Q, ini_load[0])
            buses = self.app.GetCalcRelevantObjects('*.ElmTerm')
            for bus in buses:
                self.delete_all_events()
                o_event = self.creat_events('EvtShc', 'Short Circuit')
                o_event.i_shc = 0
                o_event.time = fault_execution_time
                o_event.p_target = bus
                clc_event = self.creat_events('EvtShc', 'Clear Event')
                self.set_clear_fault(clc_event, bus, fault_clear_time)
                self.ini_and_sim(sampling_step, sim_time, sim_method='rms', net_rep='sym', record_start_time=0)
                temp_list = []
                result_df = self.get_results()
                temp_list.append(result_df)
                temp_list.append(event_label)
                temp_list.append('3-Phase Short Circuit on '+bus.loc_name)
                temp_list.append('Fault on ' + bus.loc_name+' at '+ str(fault_execution_time)+'s'+' and clear at ' + str(fault_clear_time))
                temp_list.append('Oscillation event load case  '+str(load_case_index))
                data_list.append(temp_list)
                self.app.ResetCalculation()
        col_name = ['Result Data Frame', 'event label', 'event position', 'event information', 'load scenario index']
        data_df = pd.DataFrame(data_list, columns=col_name, dtype='object')
        return data_df, loaddf

    def generate_frequency_rampup_event(self, ini_load, load_cases_for_each_level, load_level_list, load_variance, sampling_step, sim_time, rampup_start_time, rampup_duration_list, rampup_step_list, event_label):
        loaddf = self.generate_loads(load_cases_for_each_level, load_level_list, load_variance, load_name=ini_load[0], iniloadP=ini_load[1], iniloadQ=ini_load[2])
        data_list = []
        print('Starts generating frequency ramp up dataset:')
        for i in range(loaddf.shape[0]):
            load_case_index = i + 1
            print('Calculating frequency ramp up event in {} load scenario.'.format(load_case_index))
            P = loaddf.iloc[i,].real
            Q = loaddf.iloc[i,].imag
            self.set_loads(P, Q, ini_load[0])
            loads = self.app.GetCalcRelevantObjects('*.ElmLod')
            for load in loads:
                if load.typ_id:
                    load.typ_id.loddy = 100
                else:
                    print('There is no load type in {}！'.format(load.loc_name))
                    sys.exit()
                for rampup_step in rampup_step_list:
                    for rampup_duration in rampup_duration_list:
                        self.delete_all_events()
                        rp_event = self.creat_events('EvtLod','Rampup')
                        rp_event.time = rampup_start_time
                        rp_event.iopt_load = 0
                        rp_event.p_target = load
                        rp_event.iopt_type = 1
                        rp_event.t_ramp = rampup_duration
                        rp_event.dP = -rampup_step
                        self.ini_and_sim(sampling_step, sim_time, sim_method='rms', net_rep='sym', record_start_time=0)
                        temp_list = []
                        result_df = self.get_results()
                        temp_list.append(result_df)
                        temp_list.append(event_label)
                        temp_list.append(load.loc_name + ' Ramm up')
                        temp_list.append('Rampimg up'+' at '+ str(rampup_start_time)+'s' + ' with the duration ' + str(rampup_duration) + 's')
                        temp_list.append('Load ramp up load case  '+str(load_case_index))
                        data_list.append(temp_list)
                        self.app.ResetCalculation()
        col_name = ['Result Data Frame', 'event label', 'event position', 'event information', 'load scenario index']
        data_df = pd.DataFrame(data_list, columns=col_name, dtype='object')
        return data_df, loaddf
    def generate_frequency_rampdown_event(self, ini_load, load_cases_for_each_level, load_level_list, load_variance, sampling_step, sim_time, rampdown_start_time, rampdown_duration_list, rampdown_step_list, event_label):
        loaddf = self.generate_loads(load_cases_for_each_level, load_level_list, load_variance, load_name=ini_load[0], iniloadP=ini_load[1], iniloadQ=ini_load[2])
        data_list = []
        print('Starts generating frequency ramp down dataset:')
        for i in range(loaddf.shape[0]):
            load_case_index = i + 1
            print('Calculating frequency ramp down event in {} load scenario.'.format(load_case_index))
            P = loaddf.iloc[i,].real
            Q = loaddf.iloc[i,].imag
            self.set_loads(P, Q, ini_load[0])
            loads = self.app.GetCalcRelevantObjects('*.ElmLod')
            for load in loads:
                if load.typ_id:
                    load.typ_id.loddy = 100
                else:
                    print('There is no load type in {}！'.format(load.loc_name))
                    sys.exit()
                for rampdown_step in rampdown_step_list:
                    for rampdown_duration in rampdown_duration_list:
                        self.delete_all_events()
                        rp_event = self.creat_events('EvtLod','Rampdown')
                        rp_event.time = rampdown_start_time
                        rp_event.iopt_load = 0
                        rp_event.p_target = load
                        rp_event.iopt_type = 1
                        rp_event.t_ramp = rampdown_duration
                        rp_event.dP = rampdown_step
                        self.ini_and_sim(sampling_step, sim_time, sim_method='rms', net_rep='sym', record_start_time=0)
                        temp_list = []
                        result_df = self.get_results()
                        temp_list.append(result_df)
                        temp_list.append(event_label)
                        temp_list.append(load.loc_name + ' Ramm down')
                        temp_list.append('Rampimg down'+' at '+ str(rampdown_start_time)+'s' + ' with the duration ' + str(rampdown_duration) + 's')
                        temp_list.append('Load ramp down load case  '+str(load_case_index))
                        data_list.append(temp_list)
                        self.app.ResetCalculation()
        col_name = ['Result Data Frame', 'event label', 'event position', 'event information', 'load scenario index']
        data_df = pd.DataFrame(data_list, columns=col_name, dtype='object')
        return data_df, loaddf

    def generate_line_trip_event(self, ini_load, load_cases_for_each_level, load_level_list, load_variance,
                                      sampling_step, sim_time, line_trip_time, event_label):
        loaddf = self.generate_loads(load_cases_for_each_level, load_level_list, load_variance, load_name=ini_load[0],
                                     iniloadP=ini_load[1], iniloadQ=ini_load[2])
        data_list = []
        print('Starts generating line trip event dataset:')
        for i in range(loaddf.shape[0]):
            load_case_index = i + 1
            print('Calculating line trip event in {} load scenario.'.format(load_case_index))
            P = loaddf.iloc[i,].real
            Q = loaddf.iloc[i,].imag
            self.set_loads(P, Q, ini_load[0])
            lines = self.app.GetCalcRelevantObjects('*.ElmLne')
            for line in lines:
                self.delete_all_events()
                lt_event = self.creat_events('EvtOutage', 'Line_Trip')
                lt_event.time = line_trip_time
                lt_event.p_target = line
                lt_event.i_what = 0
                self.ini_and_sim(sampling_step, sim_time, sim_method='rms', net_rep='sym', record_start_time=0)
                temp_list = []
                result_df = self.get_results()
                temp_list.append(result_df)
                temp_list.append(event_label)
                temp_list.append('Trip ' + line.loc_name)
                temp_list.append('Trip ' + line.loc_name + ' at' + str(line_trip_time) + ' s')
                temp_list.append('Line trip load case  ' + str(load_case_index))
                data_list.append(temp_list)
                self.app.ResetCalculation()
        col_name = ['Result Data Frame', 'event label', 'event position', 'event information', 'load scenario index']
        data_df = pd.DataFrame(data_list, columns=col_name, dtype='object')
        return data_df, loaddf

    def concat_results(self, results_list):
        final_df = pd.concat(results_list, ignore_index=True)

        return final_df