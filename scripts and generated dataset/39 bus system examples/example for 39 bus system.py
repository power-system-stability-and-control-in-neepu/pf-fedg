#!/usr/bin/env python
# -*- coding:utf-8 -*-
# Author : Zhenglong Sun
# Data : 2022-01-15 17:33
import pfdg
import time


start = time.time()
# Please change the install path in your own PC
pf_install_path = r'D:\Digsilent15.2.1'

# This is the test system name, it should correspond with the project name in Digsilent.
project_name = '39 Bus New England System'

# Please note the bus name should be same with what in Digsilent.
# If all the buses need to be monitored, just input monitored_buses = ['all buses']
monitored_buses = ['Bus 31', 'Bus 33', 'Bus 37', 'Bus 38', 'Bus 39']

# The monitored variables include 4 types, and any combination of them can be monitored at the same time
# 'frequency'→ bus frequency '
#  'voltage angle'→ the relative bus angle shift with respect to the synchronously rotating reference.
#  'rocof'→ the rate of change of frequency for each bus
#  'voltage'→ bus voltage magnitude
monitored_variables = ['rocof']

fedg = pfdg.frequency_event_dg(pf_install_path, project_name, monitored_buses, monitored_variables)
ini_load = fedg.get_ini_load()

# The parameters will generate 1200 generator trip scenarios
# 10 generators * 3 load level * 40 load cases for each load level = 1200
gt_df, load_df0 = fedg.generate_generator_trip_event(ini_load, load_cases_for_each_level=40, load_level_list=[0.95, 1, 1.05], load_variance=2, sampling_step=0.02, sim_time=3.6, generator_trip_time=0.3, event_label=0)

# The parameters will generate 1197 load disconnection scenarios
# 19 loads * 3 load level * 21 load cases for each load level = 1197
ld_df, load_df1 = fedg.generate_load_disconnection_event(ini_load, load_cases_for_each_level=21, load_level_list=[0.95, 1, 1.05], load_variance=2, sampling_step=0.02, sim_time=3.6, load_disconnection_time=0.3, event_label=1)

# The parameters will generate 1170 fault scenarios
# 39 buses for fault * 3 load level * 10 load cases for each load level = 1170
oe_df, load_df2 = fedg.generate_oscillation_event(ini_load, load_cases_for_each_level=10, load_level_list=[0.95, 1, 1.05], load_variance=2, sampling_step=0.02, sim_time=3.6, fault_execution_time=0.3, fault_clear_time=0.33, event_label=2)

# The parameters will generate 1224 line trip scenarios
# 34 lines * 3 load level * 12 load cases for each load level = 1224

lt_df,load_df3 = fedg.generate_line_trip_event(ini_load, load_cases_for_each_level=12, load_level_list=[0.95, 1, 1.05], load_variance=2, sampling_step=0.02, sim_time=3.6, line_trip_time=0.3, event_label=3)


# The parameters will generate 1170 frequency ramp up scenarios
# 19 loads * 3 load level * 5 load cases for each load level * 4 ramp up conditions = 1140

rp_df, load_df4 = fedg.generate_frequency_rampup_event(ini_load, load_cases_for_each_level=5, load_level_list=[0.95, 1, 1.05], load_variance=2, sampling_step=0.02, sim_time=3.6, rampup_start_time =0.3, rampup_duration_list=[1, 2], rampup_step_list=[5, 10], event_label=4)

# The parameters will generate 1170 frequency ramp down scenarios
# 19 loads * 3 load level * 5 load cases for each load level * 4 ramp down conditions = 1140
rd_df, load_df5 = fedg.generate_frequency_rampdown_event(ini_load, load_cases_for_each_level=5, load_level_list=[0.95, 1, 1.05], load_variance=2, sampling_step=0.02, sim_time=3.6, rampdown_start_time=0.3, rampdown_duration_list=[1, 2], rampdown_step_list=[5, 10], event_label=5)

total_dataset = fedg.concat_results([gt_df, ld_df, oe_df, lt_df, rp_df, rd_df])
total_dataset.to_pickle("rocof total_dataset with 6 events for 39 bus system.pkl")
end = time.time()
print('The running time of this program: ', end - start)


